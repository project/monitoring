<?php
/**
 * @file
 * Contains \Drupal\monitoring\Plugin\monitoring\SensorPlugin\TwigDebugSensorPlugin.
 */

namespace Drupal\monitoring\Plugin\monitoring\SensorPlugin;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\monitoring\Attribute\SensorPlugin;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;

/**
 * Monitors the OPcache accelerated files statistics.
 */
#[SensorPlugin(
  id: 'opcache_files',
  label: new TranslatableMarkup('OPcache accelerated files'),
  addable: FALSE,
  metric_type: 'gauge',
)]
class OpCacheFilesSensorPlugin extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result) {

    if (!ini_get('opcache.enable') || !function_exists('apcu_cache_info')) {
      $sensor_result->setValue(0);
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('OPCache not enabled');
      return;
    }

    if (php_sapi_name() === 'cli') {
      $sensor_result->setValue(0);
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('CLI OPcache information not representative, skipped');
      return;
    }

    $status = opcache_get_status(FALSE);

    $max = $status['opcache_statistics']['max_cached_keys'];
    $num = $status['opcache_statistics']['num_cached_scripts'];

    $sensor_result->setValue(round(100 / $max * $num, 2));
    $sensor_result->addStatusMessage($num . ' of ' . $max);
  }

}
