<?php
/**
 * @file
 * Contains \Drupal\monitoring\Plugin\monitoring\SensorPlugin\TwigDebugSensorPlugin.
 */

namespace Drupal\monitoring\Plugin\monitoring\SensorPlugin;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\monitoring\Attribute\SensorPlugin;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;

/**
 * Monitors the apcu shared memory settings.
 */
#[SensorPlugin(
  id: 'apcu_shared_memory_expunges',
  label: new TranslatableMarkup('APCu Shared Memory expunges'),
  addable: FALSE,
  metric_type: 'gauge',
)]
class ApcuSharedMemoryExpungesSensorPlugin extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result) {

    if (!ini_get('apc.enabled') || !function_exists('apcu_cache_info')) {
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('APCU not enabled');
      return;
    }

    if (php_sapi_name() === 'cli') {
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('CLI APCU information not representative, skipped');
      return;
    }

    $info = apcu_cache_info(TRUE);
    $sensor_result->setValue($info['expunges']);
  }

}
