<?php
/**
 * @file
 * Contains \Drupal\monitoring\Plugin\monitoring\SensorPlugin\TwigDebugSensorPlugin.
 */

namespace Drupal\monitoring\Plugin\monitoring\SensorPlugin;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\monitoring\Attribute\SensorPlugin;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;

/**
 * Monitors the apcu shared memory settings.
 */
#[SensorPlugin(
  id: 'apcu_shared_memory_size',
  label: new TranslatableMarkup('APCu Shared Memory'),
  addable: FALSE,
  metric_type: 'gauge',
)]
class ApcuSharedMemorySizeSensorPlugin extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result) {

    if (!ini_get('apc.enabled') || !function_exists('apcu_cache_info')) {
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('APCU not enabled');
      return;
    }

    if (php_sapi_name() === 'cli') {
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('CLI APCU information not representative, skipped');
      return;
    }

    $shm_size = Bytes::toNumber(ini_get('apc.shm_size')) * ini_get('apc.shm_segments');

    $info = apcu_cache_info(TRUE);
    $sensor_result->setValue(round(100 / $shm_size * $info['mem_size'], 2));
    $sensor_result->addStatusMessage(ByteSizeMarkup::create($info['mem_size']) . ' of ' . ByteSizeMarkup::create($shm_size));
  }

}
