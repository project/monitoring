<?php
/**
 * @file
 * Contains \Drupal\monitoring\Plugin\monitoring\SensorPlugin\CronLastRunAgeSensorPlugin.
 */

namespace Drupal\monitoring\Plugin\monitoring\SensorPlugin;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\monitoring\Attribute\SensorPlugin;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;

/**
 * Monitors the last cron run time.
 *
 * Based on the drupal core system state cron_last.
 */
#[SensorPlugin(
  id: 'cron_last_run_time',
  label: new TranslatableMarkup('Cron Last Run Age'),
  addable: FALSE,
  metric_type: 'gauge',
)]
class CronLastRunAgeSensorPlugin extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $result) {
    $last_cron_run_before = \Drupal::time()->getRequestTime() - \Drupal::state()->get('system.cron_last');
    $result->setValue($last_cron_run_before);
  }
}
