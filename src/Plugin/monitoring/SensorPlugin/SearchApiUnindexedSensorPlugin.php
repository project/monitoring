<?php
/**
 * @file
 * Contains \Drupal\monitoring\Plugin\monitoring\SensorPlugin\SearchApiUnindexedSensorPlugin.
 */

namespace Drupal\monitoring\Plugin\monitoring\SensorPlugin;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\monitoring\Attribute\SensorPlugin;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;
use Drupal\search_api\Entity\Index;

/**
 * Monitors unindexed items for a search api index.
 *
 * Sensors are created automatically for each index.
 *
 * Every instance represents a single index.
 *
 * Once all items are processed, the value should be 0.
 */
#[SensorPlugin(
  id: 'search_api_unindexed',
  label: new TranslatableMarkup('Unindexed Search Items'),
  provider: 'search_api',
  addable: FALSE,
  metric_type: 'gauge',
)]
class SearchApiUnindexedSensorPlugin extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $result) {
    /** @var \Drupal\search_api\IndexInterface $index */
    $index = Index::load($this->sensorConfig->getSetting('index_id'));

    /* @var \Drupal\search_api\Tracker\TrackerInterface $tracker */
    $tracker = $index->getTrackerInstance();

    // Set amount of unindexed items.
    $result->setValue($tracker->getRemainingItemsCount());
  }

}
