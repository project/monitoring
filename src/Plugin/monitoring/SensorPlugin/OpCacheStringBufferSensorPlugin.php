<?php
/**
 * @file
 * Contains \Drupal\monitoring\Plugin\monitoring\SensorPlugin\TwigDebugSensorPlugin.
 */

namespace Drupal\monitoring\Plugin\monitoring\SensorPlugin;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\monitoring\Attribute\SensorPlugin;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;

/**
 * Monitors the OPcache interned string buffer.
 */
#[SensorPlugin(
  id: 'opcache_string_buffer',
  label: new TranslatableMarkup('OPcache interned string buffer'),
  addable: FALSE,
  metric_type: 'gauge',
)]
class OpCacheStringBufferSensorPlugin extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result) {

    if (!ini_get('opcache.enable') || !function_exists('apcu_cache_info')) {
      $sensor_result->setValue(0);
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('OPCache not enabled');
      return;
    }

    if (php_sapi_name() === 'cli') {
      $sensor_result->setValue(0);
      $sensor_result->setStatus(SensorResultInterface::STATUS_UNKNOWN);
      $sensor_result->setMessage('CLI OPcache information not representative, skipped');
      return;
    }

    $status = opcache_get_status(FALSE);

    $max = $status['interned_strings_usage']['buffer_size'];
    $used = $status['interned_strings_usage']['used_memory'];

    $sensor_result->setValue(round(100 / $max * $used, 2));
    $sensor_result->addStatusMessage(ByteSizeMarkup::create($used) . ' of ' . ByteSizeMarkup::create($max));
  }

}
