<?php

namespace Drupal\monitoring\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\navigation\TopBarRegion;

/**
 * Defines SensorPlugin annotation object for reference by SensorPlugin Plugins.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class SensorPlugin extends Plugin {

  /**
   * Constructs a new SensorPlugin instance.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   (optional) The human-readable label.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) The human-readable description.
   * @param string|null $provider
   *   The provider for ths plugin.
   * @param bool $addable
   *   Whether plugin instances can be created or not.
   * @param string|null $metric_type
   *   Type of metric that this sensor returns
   *   How this information is used depends on who is reading sensors.
   *
   *   Currently expected values:
   *    - counter:
   *      A counter is a metric that represents a single numerical value that
   *      monotonically increases over time. A counter resets to zero when the
   *      process restarts. Counters are used to track the number of events that
   *      occur in a system. For example, the number of requests received by a
   *      server.
   *    - gauge:
   *      A gauge is a metric that represents a single numerical value that can
   *      arbitrarily go up and down. Gauges are typically used for measured
   *      values like temperatures or current memory usage.
   * @param bool $report_execution_time
   *   Flag that it is worth to report the execution time of this sensor.
   *   Implies that the sensor does non-trivial queries and e.g. API calls where
   *   execution time is a useful metric.
   *
   * @param class-string|null $deriver
   *   (optional) The deriver class.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly ?TranslatableMarkup $description = NULL,
    public ?string $provider = NULL,
    public readonly bool $addable = TRUE,
    public ?string $metric_type = NULL,
    public bool $report_execution_time = FALSE,
    public readonly ?string $deriver = NULL,

  ) {}
}
