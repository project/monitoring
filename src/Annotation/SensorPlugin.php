<?php

/**
 * @file
 * Contains \Drupal\monitoring\Annotation\SensorPlugin.
 */

namespace Drupal\monitoring\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines SensorPlugin annotation object for reference by SensorPlugin Plugins.
 *
 * @Annotation
 */
class SensorPlugin extends Plugin {

   /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation (optional)
   */
  public $description = '';

  /**
   * The provider of the annotated class.
   *
   * @var string
   */
  public $provider;

  /**
   * Whether plugin instances can be created or not.
   *
   * @var boolean
   */
  public $addable;

  /**
   * Type of metric that this sensor returns
   *
   * How this information is used depends on who is reading sensors.
   *
   * Currently expected values:
   *  * counter:
   *    A counter is a metric that represents a single numerical value that
   *    monotonically increases over time. A counter resets to zero when the
   *    process restarts. Counters are used to track the number of events that
   *    occur in a system. For example, the number of requests received by a
   *    server.
   *  * gauge:
   *    A gauge is a metric that represents a single numerical value that can
   *    arbitrarily go up and down. Gauges are typically used for measured
   *    values like temperatures or current memory usage.
   *
   * @var string|null
   */
  public ?string $metric_type = NULL;

  /**
   * Flag that it is worth to report the execution time of this sensor.
   *
   * Implies that the sensor does non-trivial queries and e.g. API calls
   * where execution time is a useful metric.
   *
   * @var bool
   */
  public bool $report_execution_time = FALSE;
}
