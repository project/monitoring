<?php

namespace Drupal\Tests\monitoring\Kernel;

use Drupal\monitoring\Entity\SensorConfig;

/**
 * Kernel tests for the monitoring response time plugin.
 *
 * @group monitoring
 */
class MonitoringResponseTimeSensorTest extends MonitoringUnitTestBase {

  /**
   * Test the response time sensor plugin.
   */
  public function testResponseTimeSensorPlugin() {
    $sensor = SensorConfig::create([
      'id' => 'response_time_test',
      'label' => 'Response time',
      'plugin_id' => 'response_time',
      'caching_time' => 86400,
      'value_type' => 'number',
      'value_label' => 'ms',
      'thresholds' => [
        'type' => 'exceeds',
        'warning' => 3000,
        'critical' => 3200,
      ],
      'settings' => [
        'main_metric' => 'percentile_90',
        'lines' => 25,
        'log_file' => \Drupal::service('extension.list.module')->getPath('monitoring') . '/tests/fixtures/example.php.access.log',
      ],
    ]);
    $sensor->save();

    $sensor_result = $this->runSensor('response_time_test');
    $this->assertTrue($sensor_result->isOk());
    $this->assertEquals('2935 ms, Average: 1010 ms, 90%: 2935 ms, 95%: 4626ms, start date: 2021-11-13T15:36:54Z', $sensor_result->getMessage());

    // More lines than the length of the file.
    $sensor->set('settings', [
      'main_metric' => 'percentile_90',
      'lines' => 2500,
      'log_file' => __DIR__ . '/../../fixtures/example.php.access.log',
    ]);
    $sensor->save();
    \Drupal::state()->delete('monitoring.response_time_test');
    $sensor_result = $this->runSensor('response_time_test');
    $this->assertTrue($sensor_result->isOk());
    $this->assertEquals('2856 ms, Average: 975 ms, 90%: 2856 ms, 95%: 4540ms, start date: 2021-11-13T15:23:36Z', $sensor_result->getMessage());

    // Change thresholds to get the warning status.
    $sensor->set('thresholds', [
      'type' => 'exceeds',
      'warning' => '2800',
      'critical' => 3000,
    ]);
    $sensor->save();
    $sensor_result = $this->runSensor('response_time_test');
    $this->assertTrue($sensor_result->isWarning());
    $this->assertEquals('2856 ms, exceeds 2800, Average: 975 ms, 90%: 2856 ms, 95%: 4540ms, start date: 2021-11-13T15:23:36Z', $sensor_result->getMessage());

    // Change thresholds to get the critical status.
    $sensor->set('thresholds', [
      'type' => 'exceeds',
      'warning' => '2500',
      'critical' => 2800,
    ]);
    $sensor->save();
    $sensor_result = $this->runSensor('response_time_test');
    $this->assertTrue($sensor_result->isCritical());
    $this->assertEquals('2856 ms, exceeds 2800, Average: 975 ms, 90%: 2856 ms, 95%: 4540ms, start date: 2021-11-13T15:23:36Z', $sensor_result->getMessage());

    // New metric.
    $sensor->set('settings', [
      'main_metric' => 'percentile_95',
      'lines' => 25,
      'log_file' => __DIR__ . '/../../fixtures/example.php.access.log',
    ]);
    $sensor->save();
    $sensor_result = $this->runSensor('response_time_test');
    $this->assertEquals('4626', $sensor_result->getValue());
    $this->assertTrue($sensor_result->isCritical());
    $this->assertEquals('4626 ms, exceeds 2800, Average: 1010 ms, 90%: 2935 ms, 95%: 4626ms, start date: 2021-11-13T15:36:54Z', $sensor_result->getMessage());

    // New metric.
    $sensor->set('settings', [
      'main_metric' => 'average',
      'lines' => 25,
      'log_file' => __DIR__ . '/../../fixtures/example.php.access.log',
    ]);
    $sensor->save();
    $sensor_result = $this->runSensor('response_time_test');
    $this->assertEquals('1010', $sensor_result->getValue());
    $this->assertTrue($sensor_result->isOk());
    $this->assertEquals('1010 ms, Average: 1010 ms, 90%: 2935 ms, 95%: 4626ms, start date: 2021-11-13T15:36:54Z', $sensor_result->getMessage());

    // Set invalid file.
    $sensor->set('settings', [
      'main_metric' => 'percentile_90',
      'lines' => 25,
      'log_file' => '/example.php.access.log',
    ]);
    $sensor->save();
    $sensor_result = $this->runSensor('response_time_test');
    $this->assertTrue($sensor_result->isCritical());
    $this->assertEquals('Exception: Log file /example.php.access.log not found.', $sensor_result->getMessage());

    // Empty file.
    $sensor->set('settings', [
      'main_metric' => 'percentile_90',
      'lines' => 25,
      'log_file' => __DIR__ . '/../../fixtures/empty.php.access.log',
    ]);
    $sensor->save();
    $sensor_result = $this->runSensor('response_time_test');
    $this->assertTrue($sensor_result->isWarning());
    $this->assertEquals('Log file ' . __DIR__ . '/../../fixtures/empty.php.access.log is empty.', $sensor_result->getMessage());

  }

}
