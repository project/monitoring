<?php

namespace Drupal\Tests\monitoring\Functional;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\monitoring\Entity\SensorConfig;

/**
 * Tests the view display sensor.
 *
 * @group monitoring
 */
class MonitoringViewDisplayTest extends MonitoringTestBase {

  protected static $modules = ['views', 'node'];

  /**
   * Tests the view display sensor.
   *
   * @see \Drupal\monitoring\Plugin\monitoring\SensorPlugin\ViewDisplayAggregatorSensorPlugin
   */
  public function testViewDisplaySensor() {
    $account = $this->drupalCreateUser(array('administer monitoring', 'monitoring reports'));
    $this->drupalLogin($account);

    // Add sensor type views display aggregator.
    $this->drupalGet('admin/config/system/monitoring/sensors/add');
    $this->submitForm(array(
      'label' => 'All users',
      'id' => 'view_user_count',
      'plugin_id' => 'view_display_aggregator',
    ), 'Select sensor');
    // Select view and display and save.
    $this->assertSession()->pageTextContains('Sensor plugin settings');
    $this->submitForm(array(
      'description' => 'Count all users through the users view.',
      'value_label' => 'Users',
      'caching_time' => 0,
      'settings[view]' => 'user_admin_people',
    ), 'Select view');
    $this->submitForm(array(
      'settings[display]' => 'default',
    ), 'Save');
    $this->assertSession()->pageTextContains(new FormattableMarkup('Sensor @label saved.', array('@label' => 'All users')));

    // Check the value type has the default value.
    $sensor_config = SensorConfig::load('view_user_count');
    $this->assertEquals($sensor_config->getValueType(), 'number');
    // Edit and check selection.
    $this->drupalGet('admin/config/system/monitoring/sensors/view_user_count');
    $this->assertTrue($this->assertSession()->optionExists('edit-settings-view', 'user_admin_people')->hasAttribute('selected'));
    $this->assertTrue($this->assertSession()->optionExists('edit-settings-display', 'default')->hasAttribute('selected'));

    // Call sensor and verify status and message.
    $result = $this->runSensor('view_user_count');
    $this->assertTrue($result->isOk());
    $this->assertEquals($result->getMessage(), '2 users');

    // Create an additional user.
    $this->drupalCreateUser();

    // Call sensor and verify status and message.
    $result = $this->runSensor('view_user_count');
    $this->assertTrue($result->isOk());
    $this->assertEquals($result->getMessage(), '3 users');
  }

}
