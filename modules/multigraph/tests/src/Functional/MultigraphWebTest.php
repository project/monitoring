<?php
/**
 * @file
 * Contains \Drupal\monitoring_multigraph\Tests\MultigraphWebTest
 */

namespace Drupal\Tests\monitoring_multigraph\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Multigraph forms (add/edit/delete).
 *
 * @group monitoring
 */
class MultigraphWebTest extends BrowserTestBase {

  /**
   * User object.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'node',
    'monitoring',
    'monitoring_multigraph',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Configures test base and executes test cases.
   */
  public function testMultigraphForm() {
    // Create and log in our user.
    $this->adminUser = $this->drupalCreateUser([
      'administer monitoring',
    ]);

    $this->drupalLogin($this->adminUser);

    $this->doTestMultigraphAdd();
    $this->doTestMultigraphEdit();
    $this->doTestMultigraphDelete();
  }

  /**
   * Tests multigraph creation.
   */
  public function doTestMultigraphAdd() {
    // Add a few sensors.
    $values = [
      'label' => $this->randomString(),
      'id' => 'multigraph_123',
      'description' => $this->randomString(),
      'sensor_add_select' => 'dblog_404',
    ];
    $this->drupalGet('admin/config/system/monitoring/multigraphs/add');
    $this->submitForm($values, 'Add sensor');
    $this->assertSession()->pageTextContains(t('Sensor "Page not found errors" added. You have unsaved changes.'));

    $this->submitForm([
      'sensor_add_select' => 'user_failed_logins',
    ], 'Add sensor');
    $this->assertSession()->pageTextContains(t('Sensor "Failed user logins" added. You have unsaved changes.'));

    $this->submitForm([
      'sensor_add_select' => 'user_successful_logins',
    ], 'Add sensor');
    $this->assertSession()->pageTextContains(t('Sensor "Successful user logins" added. You have unsaved changes.'));

    // And last but not least, change all sensor label values and save form.
    $this->submitForm([
      'sensors[dblog_404][label]' => 'Page not found errors (test)',
      'sensors[user_failed_logins][label]' => 'Failed user logins (test)',
      'sensors[user_successful_logins][label]' => 'Successful user logins (test)',
    ], 'Save');
    $this->assertSession()->pageTextContains(t('Multigraph settings saved.'));
    $this->assertSession()->pageTextContains('Page not found errors (test), Failed user logins (test), Successful user logins (test)');
  }

  /**
   * Tests multigraph editing.
   *
   * Tests all changeable input fields.
   */
  public function doTestMultigraphEdit() {
    // Go to multigraph overview and test editing pre-installed multigraph.
    $this->drupalGet('admin/config/system/monitoring/multigraphs');
    // Check label, description and sensors (before editing).
    $this->assertSession()->pageTextContains('Watchdog severe entries');
    $this->assertSession()->pageTextContains('Watchdog entries with severity Warning or higher');
    $this->assertSession()->pageTextContains('404, Alert, Critical, Emergency, Error');

    // Edit.
    $this->drupalGet('admin/config/system/monitoring/multigraphs/watchdog_severe_entries');
    $this->assertSession()->pageTextContains('Edit Multigraph');

    // Change label, description and add a sensor.
    $values = [
      'label' => 'Watchdog severe entries (test)',
      'description' => 'Watchdog entries with severity Warning or higher (test)',
      'sensor_add_select' => 'user_successful_logins',
    ];
    $this->submitForm($values, 'Add sensor');
    $this->assertSession()->pageTextContains('Sensor "Successful user logins" added. You have unsaved changes.');

    // Remove a sensor.
    $this->getSession()->getPage()->pressButton('remove_dblog_404');
    // (drupalPostAjaxForm() lets us target the button precisely.)
    $this->assertSession()->pageTextContains(t('Sensor "Page not found errors" removed. You have unsaved changes.'));
    $this->submitForm([], 'Save');
    $this->drupalGet('admin/config/system/monitoring/multigraphs/watchdog_severe_entries');

    // Change weights and save form.
    $this->submitForm([
      'sensors[user_successful_logins][weight]' => -2,
      'sensors[dblog_event_severity_error][weight]' => -1,
      'sensors[dblog_event_severity_critical][weight]' => 0,
      'sensors[dblog_event_severity_emergency][weight]' => 1,
      'sensors[dblog_event_severity_alert][weight]' => 2,
    ], 'Save');
    $this->assertSession()->pageTextContains(t('Multigraph settings saved.'));

    // Go back to multigraph overview and check changed values.
    $this->drupalGet('admin/config/system/monitoring/multigraphs');
    $this->assertSession()->pageTextContains('Watchdog severe entries (test)');
    $this->assertSession()->pageTextContains('Watchdog entries with severity Warning or higher (test)');
    $this->assertSession()->pageTextContains('Successful user logins, Error, Critical, Emergency, Alert');
  }

  /**
   * Tests multigraph deletion.
   */
  public function doTestMultigraphDelete() {
    // Go to multigraph overview and check for pre-installed multigraph.
    $this->drupalGet('admin/config/system/monitoring/multigraphs');
    // Check label and description (before deleting).
    $this->assertSession()->pageTextContains('Watchdog severe entries');
    $this->assertSession()->pageTextContains('Watchdog entries with severity Warning or higher');
    $this->drupalGet('admin/config/system/monitoring/multigraphs/watchdog_severe_entries/delete');

    // Delete.
    $this->submitForm([], 'Delete');
    $this->assertSession()->pageTextContains('The Watchdog severe entries (test) multigraph has been deleted');

    // Go back to multigraph overview and check that multigraph is deleted.
    $this->drupalGet('admin/config/system/monitoring/multigraphs');
    $this->assertSession()->pageTextNotContains('Watchdog severe entries');
    $this->assertSession()->pageTextNotContains('Watchdog entries with severity Warning or higher');
  }
}
