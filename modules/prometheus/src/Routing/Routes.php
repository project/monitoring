<?php

namespace Drupal\monitoring_prometheus\Routing;

use Drupal\Core\Authentication\AuthenticationCollectorInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\monitoring_prometheus\Controller\MetricsController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines dynamic routes.
 */
class Routes implements ContainerInjectionInterface {

  /**
   * The authentication collector.
   *
   * @var \Drupal\Core\Authentication\AuthenticationCollectorInterface
   */
  protected AuthenticationCollectorInterface $authCollector;

  /**
   * Instantiates a Routes object.
   *
   * @param \Drupal\Core\Authentication\AuthenticationCollectorInterface $auth_collector
   *   The authentication provider collector.
   */
  public function __construct(AuthenticationCollectorInterface $auth_collector) {
    $this->authCollector = $auth_collector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('authentication_collector')
    );
  }

  /**
   * Provides the routes.
   */
  public function routes(): RouteCollection {
    $collection = new RouteCollection();

    $route = new Route('/metrics', [
      RouteObjectInterface::CONTROLLER_NAME => MetricsController::class . '::metrics',
    ]);
    $route->setRequirement('_custom_access', '\Drupal\monitoring_prometheus\Controller\MetricsController::ipAccess');
    $route->setRequirement('_permission', 'access monitoring prometheus metrics');
    $route->setDefault('_disable_route_normalizer', TRUE);
    $route->setMethods(['GET']);
    $route->addOptions([
      '_auth' => array_keys($this->authCollector->getSortedProviders()),
    ]);
    $collection->add('monitoring_prometheus.metrics', $route);

    return $collection;
  }

}
