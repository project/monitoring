<?php

namespace Drupal\Tests\monitoring_prometheus\Functional;

use Drupal\Tests\monitoring\Functional\MonitoringTestBase;

/**
 * Integration tests for the prometheus metrics page.
 *
 * @group monitoring
 */
class PrometheusTest extends MonitoringTestBase {

  /**
   * {{@inheritdoc}}
   */
  protected static $modules = ['dblog', 'image', 'node', 'taxonomy', 'file', 'monitoring_prometheus'];

  /**
   * Tests individual sensors.
   */
  public function testMetrics() {

    $this->drupalGet('/metrics');
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->createUser(['access monitoring prometheus metrics', 'administer monitoring']);
    $this->drupalLogin($user);

    $this->drupalGet('/metrics');
    $this->assertSession()->statusCodeEquals(200);
    if (version_compare(\Drupal::VERSION, '11.0.99', '>')) {
      $this->assertSession()->responseHeaderContains('X-Drupal-Cache', 'UNCACHEABLE');
      $this->assertSession()->responseHeaderContains('X-Drupal-Dynamic-Cache', 'UNCACHEABLE');
    }

    // Assert output of a generic sensor, assert no execution time is displayed.
    $this->assertSession()->responseContains('# HELP drupal_core_cron_last_run_age_seconds Last cron run age');
    $this->assertSession()->responseContains('# TYPE drupal_core_cron_last_run_age_seconds gauge');
    $this->assertSession()->responseMatches('/drupal_core_cron_last_run_age_seconds \d+/');
    $this->assertSession()->responseNotContains('drupal_core_cron_last_run_age_execution_time');

    // A sensor that opts into execution time has that information displayed.
    $this->assertSession()->responseMatches('/drupal_temporary_files_usages_execution_time_seconds 0\.\d+/');

    // Non-metric sensor are not listed.
    $this->assertSession()->responseNotContains('_requirements_system');

    // Assert sensor count metrics.
    $this->assertSession()->responseContains('# HELP drupal_sensors_total Sensor count by status');
    $this->assertSession()->responseContains('# TYPE drupal_sensors_total gauge');
    $this->assertSession()->responseMatches('/drupal_sensors_total \d+/');
    $this->assertSession()->responseMatches('/drupal_sensors_total{status="OK"} \d+/');
    $this->assertSession()->responseMatches('/drupal_sensors_total{status="CRITICAL"} \d+/');

    // Custom labels.
    $this->drupalGet('admin/config/system/monitoring/settings');
    $this->submitForm(['custom_labels' => "foo=bar\r\nsite=[site:name]"], 'Save configuration');
    $this->drupalGet('/metrics');
    $sitename = $this->config('system.site')->get('name');
    $this->assertSession()->responseMatches('/drupal_sensors_total{foo="bar",site="' . $sitename .'"} \d+/');
    $this->assertSession()->responseMatches('/drupal_sensors_total{foo="bar",site="' . $sitename . '",status="CRITICAL"} \d+/');
  }

  /**
   * @covers \Drupal\monitoring_prometheus\Controller\MetricsController::ipAccess
   */
  public function testIpAccess() {
    $user = $this->createUser(['access monitoring prometheus metrics', 'administer monitoring']);
    $this->drupalLogin($user);
    $this->drupalGet('/metrics');
    $this->assertSession()->statusCodeEquals(200);

    // We just confirm, that not allowed if allowed_ips is set
    $this->drupalGet('admin/config/system/monitoring/settings');
    $this->submitForm(['allowed_ips' => "1.1.1.1\r\n2.2.2.2"], 'Save configuration');
    $this->drupalGet('/metrics');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/config/system/monitoring/settings');
    $this->submitForm(['allowed_ips' => ""], 'Save configuration');
    $this->drupalGet('/metrics');
    $this->assertSession()->statusCodeEquals(200);
  }

}
