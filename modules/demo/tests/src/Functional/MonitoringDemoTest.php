<?php

namespace Drupal\Tests\monitoring_demo\Functional;

use Drupal\Tests\monitoring\Functional\MonitoringTestBase;

/**
 * Tests the demo module for monitoring.
 *
 * @group monitoring
 */
class MonitoringDemoTest extends MonitoringTestBase {

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['monitoring_demo'];

  /**
   * Asserts the demo instructions on the frontpage.
   */
  public function testInstalled() {
    $this->drupalGet('');
    $this->assertSession()->pageTextContains('Monitoring');
    $this->assertSession()->pageTextContains(t('Welcome to the Monitoring demo installation.'));
    $this->assertSession()->linkExists(t('Monitoring sensors overview'));
    $this->assertSession()->linkExists(t('Monitoring sensors settings'));
    $this->assertSession()->pageTextContains(t('Sensor example: "Installed modules"'));
    $this->assertSession()->linkExists(t('Configure'));
    $this->assertSession()->linkExists(t('Uninstall'), 0);
    $this->assertSession()->linkExists(t('Uninstall'), 1);
    $this->assertSession()->pageTextContains(t('Drush integration - open up your console and type in # drush monitoring-sensor-config'));
  }

}
